// server Dependencies

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order")
const addToCartRoutes = require("./routes/addToCart")
const wishListRoutes = require("./routes/wishList")


// Server setup
const app = express();

// Environment Setup

const port = 4000;

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// This one allows all resources (frontend app) to access our backend application 
app.use(cors());

// mongoose Connection

mongoose.connect("mongodb+srv://ronnelaldrinmariano:admin123@batch295.lar8wfu.mongodb.net/capstone-2?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);


let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error'));
db.once('open', ()=> console.log('Connected to MongoDB Atlas.'));


// Backend Routes
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)
app.use("/add-to-cart", addToCartRoutes)
app.use("/wish-list", wishListRoutes)











if(require.main === module) {
    app.listen(process.env.PORT || port, () => {console.log(`Server is now running in port ${process.env.PORT || port}.`)});
};
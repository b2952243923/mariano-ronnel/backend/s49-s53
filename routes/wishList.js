// Dependencies and Modules 
const express = require('express');
const wishListController = require("../controllers/wishList");
const auth = require('../auth');

// destructure the auth file: 
// here we use the verify and verifyAdmin as auth middlwares.
const { verify, verifyAdmin} = auth;

const router = express.Router();

// Route for creating wishlist 
router.post('/create-wish-list', verify, wishListController.createWishList);

// Add a product to the wishlist
router.post('/:wishlistId/products/:productId', verify, wishListController.addProduct);

// Remove a product from the wishlist
router.delete('/:wishlistId/products/:productId', verify, wishListController.removeProduct);

// Get all User wishlists
router.get('/', verify, wishListController.getAllWishLists);

// Get all wishlists (Only admin can access)
router.get('/all-wishlist-created', verify, verifyAdmin, wishListController.getAllWishLists);





module.exports = router;
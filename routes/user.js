// Dependencies and Modules 
const express = require('express');
const userController = require("../controllers/user");
const auth = require('../auth');

// destructure the auth file: 
// here we use the verify and verifyAdmin as auth middlwares.
const { verify, verifyAdmin} = auth;

const router = express.Router();


// Route for registration

router.post("/register", userController.userRegistration);

// Login User
router.post("/login", userController.loginUser);

// Route to set a user as admin
router.put('/updateAdmin',verify, verifyAdmin, userController.createNewAdmin)

//  Route to retrive users details 
router.get("/userdetails", userController.getUserDetails);

// Route for forgotpassword and generate a new one 
router.put('/forgot-password',verify,  userController.forgotPassword);

// Route for checking if user email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving authenticated user details
router.get("/details", verify, userController.getProfile)

// Update user profile route
router.put('/profile', verify, userController.updateProfile);








module.exports = router;



const express = require('express');
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

// Route to create an order or place an order
router.post('/checkout', verify, orderController.createOrder);

// Route to retrieve orders for a specific user
router.get('/user-orders', verify, orderController.getUserOrders);

// Route to retrieve all orders for admin
router.get('/all-orders',verify, verifyAdmin, orderController.getAllOrders);

module.exports = router;

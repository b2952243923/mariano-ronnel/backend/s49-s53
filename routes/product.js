// Dependencies and Modules 
const express = require('express');
const productController = require("../controllers/product");
const auth = require('../auth');

// destructure the auth file: 
// here we use the verify and verifyAdmin as auth middlwares.
const { verify, verifyAdmin} = auth;

const router = express.Router();

// Route to create product
router.post('/createproduct',verify, verifyAdmin, productController.createProduct);

// Retrieve all products
router.get('/retrieveallproducts', productController.getAllProducts);

// Retrieve active products
router.get('/retrieveactiveproducts',verify, productController.getActiveProducts);

// Retrieve single product
router.get("/:id", productController.getSingleProduct);

// updating product information 
router.put("/:id",verify, verifyAdmin, productController.updateProductInformation);

// Archive Products

router.put("/:id/archive", verify, verifyAdmin, productController.archiveProducts);


// activate Products 

router.put("/:id/activateproduct", verify, verifyAdmin, productController.activateProducts);

// Route to search for courses by course name
router.post('/search', productController.searchByProductName);


router.get("/home", productController.getAllActive);



// router.get("/all", productController.getAllProducts);

//[ACTIVITY] Search Courses By Price Range
router.post('/searchByPrice', productController.searchCoursesByPriceRange);

// Delete product
router.delete("/:id", verify, verifyAdmin, productController.deleteProduct);



module.exports = router;
const mongoose = require('mongoose');

const wishlistSchema = new mongoose.Schema
(
  {
  user: { 
    type: String, 
    required: [true, 'user is require']  
  },
  product: [
    { 
      type: String
    }],
  createdOn: 
    { 
        type: Date, 
        default: new Date()
    }
  }
);

module.exports = mongoose.model('WishList', wishlistSchema);
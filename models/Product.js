const mongoose = require('mongoose');


// Define the Product schema
const productSchema = new mongoose.Schema({
    name: 
    { 
        type: String, 
        required: [true, "name is required"] 
    },
    description: 
    { 
        type: String, 
        required: [true, "description is required"]  
    },
    price: 
    { 
        type: Number, 
        required: [true, "price is required"]  
    },
    isActive: 
    { 
        type: Boolean, 
        default: true
    },
    createdOn: 
    { 
        type: Date, 
        default: new Date()
    },
    imgUrl: {
      type: String,
      required: [true, "imgUrl is required"],
    }
  });

  module.exports = mongoose.model('Product', productSchema);
  
const mongoose = require('mongoose');

// Define the Order schema
const orderSchema = new mongoose.Schema({
    userId: 
    { 
        type: String, 
        required: [true, "userId is required"] 
    },
    products: 
    [{
      productId: 
        { 
            type: String, 
            required: [true, "productId is required"] 
        },
      quantity: 
        { 
            type: Number, 
            required: [true, "quantity is required"] 
        }
    }],
      createdOn: { 
      type: Date, 
      default: Date.now() 
      },
    totalAmount: 
    { 
        type: Number, 
        required: [true, "totalAmount is required"] 
    }
  });
  

  module.exports = mongoose.model('Order', orderSchema);


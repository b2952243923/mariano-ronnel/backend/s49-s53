const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

// Create a new order
module.exports.createOrder = async (req, res) => {
  try {
    const { userId, products } = req.body;

    // Check if the user exists
    const user = await User.findById(userId);
    if (!user || user.isAdmin) {
      return res.status(404).json(false);
    }

    // Get all the products by their IDs from the Product model
    const productIds = products.map((product) => product.productId);
    const fetchedProducts = await Product.find({ _id: { $in: productIds } });

    // Calculate the total amount based on the fetched products
    let totalAmount = 0;
    products.forEach((product) => {
      const matchedProduct = fetchedProducts.find((p) => p._id.toString() === product.productId);
      if (matchedProduct) {
        totalAmount += matchedProduct.price * product.quantity;
      }
    });

    // Create the order
    const order = new Order({ userId, products,totalAmount });
    const savedOrder = await order.save();

    res.status(201).json(savedOrder);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

// Retrieve all orders for a specific user
module.exports.getUserOrders = async (req, res) => {
  const userId = req.user.id; // Assuming the user ID is available in the req.user.id property

  try {
    // Find the orders for the user
    const orders = await Order.find({ userId });

    if (orders.length === 0) {
      return res.status(404).json({ message: 'No orders found for the user' });
    }

    res.status(200).json({ orders });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving user orders' });
  }
};

// Retrieve all orders for admin
module.exports.getAllOrders = async (req, res) => {
  try {
    // Check if the user making the request is an admin
    if (!req.user.isAdmin) {
      return res.status(403).json({ error: 'Access denied' });
    }

    // Find all orders
    const orders = await Order.find();

    res.json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
};

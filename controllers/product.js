// Dependencies and Modules
const Product = require("../models/Product");


// Create a product (admin only)

module.exports.createProduct = async (req, res) => {
    try {
      let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        imgUrl: req.body.imgUrl
      });
  
      const product = await newProduct.save();
      res.send(product);
    } catch (error) {
      res.send(error)
    }
  };

  // Retrieve all products
  module.exports.getAllProducts = async (req, res) => {
    try {
      const result = await Product.find({});
      return res.send(result);
    } catch (err) {
      return res.send(err);
    }
  };

    // Retrieve active products
    module.exports.getActiveProducts = async (req, res) => {
        try {
          const result = await Product.find({isActive: true});
          return res.send(result);
        } catch (err) {
          return res.send(err);
        }
      };
  
      // Retrieve single product
      module.exports.getSingleProduct = async (req, res) => {
        try {
            const result = await Product.findById(req.params.id);
            return res.send(result);
        } catch (err) {
            return res.send(err);
        }
    };
    // Update Product Information 
    module.exports.updateProductInformation = async (req, res) => {
      try {
        let updatedProduct = {
          name: req.body.name,
          description: req.body.description,
          price: req.body.price
        }
        const product = await Product.findByIdAndUpdate(req.params.id, updatedProduct);
        if (!product) {
          return res.status(404).send("Product not found");
        }
        return res.send(true);
      } catch (error) {
        return res.status(500).send(error);
      }
    };


    
  // Archive product
  module.exports.archiveProducts = async (req, res) => {
    let updateActiveProduct = {
      isActive: false
    }
    try {
      const course = await Product.findByIdAndUpdate(req.params.id, updateActiveProduct);
      return res.send(true);
    } catch (error) {
      return res.send(false);
    }
  };

   // Activate product
   module.exports.activateProducts = async (req, res) => {
    let activateProduct = {
      isActive: true
    }
    try {
      const course = await Product.findByIdAndUpdate(req.params.id, activateProduct);
      return res.send(true);
    } catch (error) {
      return res.send(false);
    }
  };


  
module.exports.getAllActive = (req, res) => {
	return Course.find({ isActive: true }).then(result => {
		// console.log(result)
		return res.send(result)
	})
	.catch(err => res.send(err))
};

// Controller action to search for courses by course name
// Search products by name
module.exports.searchByProductName = async (req, res) => {
  try {
    const { name } = req.body;
    const products = await Product.find({
      name: { $regex: name, $options: "i" },
    });
    res.json(products);
  } catch (error) {
    res.status(500).json({ error: "Error searching for products" });
  }
};

// empty {} will return all the documents from the courses collection.
// module.exports.getAllProducts = (req, res) => {
// 	return Course.find({}).then(result => {
// 		// console.log(result)
// 		return res.send(result);
// 	})
// 	.catch(err => res.send(err))
// };


module.exports.searchCoursesByPriceRange = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

       // Find courses within the price range
    const products = await Product.find({
     price: { $gte: minPrice, $lte: maxPrice }
   });
   
   res.status(200).json({ products });
 } catch (error) {
   res.status(500).json({ error: 'An error occurred while searching for courses' });
 }
};

// Delete product
module.exports.deleteProduct = async (req, res) => {
  try {
    const productId = req.params.id;

    // Check if the product exists
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: "Product not found" });
    }

    // Perform the deletion
    await Product.findByIdAndDelete(productId);

    // Return success response
    res.json({ success: true });
  } catch (error) {
    res.status(500).json({ error: "Error deleting product" });
  }
};

// Dependencies and Modules
const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.userRegistration = (req, res) => {
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10)
    })

    return newUser.save().then((user, error)=> {
        if(error) {
            return res.send(false)
        } else {
            return res.send(true)
        }
    }) 
};

// login user
module.exports.loginUser = (req, res) => {
    return User.findOne({email: req.body.email}).then(result => {
        // If User does not exist
        if(result == null) {
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            if(isPasswordCorrect){ 
                return res.send({access: auth.createAccessToken(result)})
            } else {
                return res.send(false);
            }
        }
    }).catch(err => res.send(err));
};


module.exports.createNewAdmin =  async (req, res) => {
    const { userId } = req.body; // Destructure userId from req.body
  
  try {
    const adminUserId = req.user.id; // Assuming the admin token includes the user id
  
    const adminUser = await User.findById(adminUserId); // Fetch the admin user
    
    if (!adminUser.isAdmin) { // Check if the admin user is authorized
      return res.status(401).json({ message: 'Unauthorized' });
    }

    const user = await User.findById(userId); // Fetch the user to be updated

    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    user.isAdmin = true; // Update the user as admin
    await user.save();

    return res.json({ message: 'User updated as admin successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};


// Retrieve users details
module.exports.getUserDetails = async (req, res) => {
  try {
    const result = await User.findById(req.user.id);
    result.password = ""
    return res.send(result);
  } catch (err) {
    return res.send(err);
  }
};



// Controller for forgot password and updating a newone
module.exports.forgotPassword = async (req, res) => {
    try {
      const { newPassword } = req.body;
      const { id } = req.user;
      
      const hashedPassword = await bcrypt.hash(newPassword, 10);
  
      await User.findByIdAndUpdate(id, { password: hashedPassword });
  
      return res.status(200).json({ message: 'Password reset successfully' });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Internal server error' });
    }
  };

  module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
  
      // The "find" metho
      // returns a record if a match is found.
      if(result.length > 0) {
        return true; //"Duplicate email found"
      } else {
        return false;
      }
    })
  };


  module.exports.getProfile = (req, res) => {


    return User.findById(req.user.id).then(result => {
  
      // Changes the value of the user's password to an empty string when returned to the frontend
      // Not doing so will expose the user's password which will also not be needed in other parts of our application
      // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
      result.password = "";
  
      // Returns the user information with the password as an empty string
      return res.send(result);
  
    })
    .catch(err => res.send(err))
  };


  module.exports.updateProfile = async (req, res) => {
    try {
      // Get the user ID from the authenticated token
      const userId = req.user.id;
  
      // Retrieve the updated profile information from the request body
      const { firstName, lastName } = req.body;
  
      // Update the user's profile in the database
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { firstName, lastName },
        { new: true }
      );
  
      res.json(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Failed to update profile' });
    }
  };

  module.exports.updateProfile = async (req, res) => {
    try {
      // Get the user ID from the authenticated token
      const userId = req.user.id;
  
      // Retrieve the updated profile information from the request body
      const { firstName, lastName } = req.body;
  
      // Update the user's profile in the database
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { firstName, lastName },
        { new: true }
      );
  
      res.json(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Failed to update profile' });
    }
  };
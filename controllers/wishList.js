// Dependencies and Modules
const wishList = require("../models/WishList");
const Product = require("../models/Product");
const User = require("../models/User");

// Create a new wishlist
module.exports.createWishList = async (req, res) => {
    const { userId } = req.body;
    
    try {
      const user = await User.findById(userId);
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      const wishlist = await wishList.create({ user: userId });
      res.status(201).json(true);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Failed to create wishlist' });
    }
  };


  // Add a product to the wishlist
module.exports.addProduct = async (req, res) => {
    const { wishlistId, productId } = req.params;
    
    try {
      const wishlist = await wishList.findById(wishlistId);
      const product = await Product.findById(productId);
  
      if (!wishlist || !product) {
        return res.status(404).json({ error: 'Wishlist or product not found' });
      }
  
      if (wishlist.product.includes(productId)) {
        return res.status(400).json({ error: 'Product already exists in the wishlist' });
      }
  
      wishlist.product.push(productId);
      await wishlist.save();
      res.status(200).json(wishlist);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Failed to add product to wishlist' });
    }
  };


  // Remove a product from the wishlist
    module.exports.removeProduct = async (req, res) => {
        const { wishlistId, productId } = req.params;
    
        try {
        const wishlist = await wishList.findById(wishlistId);
        const product = await Product.findById(productId);
    
        if (!wishlist || !product) {
            return res.status(404).json({ error: 'Wishlist or product not found' });
        }
    
        if (!wishlist.product.includes(productId)) {
            return res.status(400).json({ error: 'Product does not exist in the wishlist' });
        }
    
        wishlist.product = wishlist.product.filter((pId) => pId.toString() !== productId);
        await wishlist.save();
        res.status(200).json(wishlist);
        } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Failed to remove product from wishlist' });
        }
  };

    // Get all Users wishlists
    module.exports.getAllWishLists = async (req, res) => {
        try {
        const wishlists = await wishList.find({});
        res.status(200).json(wishlists);
        } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Failed to get wishlists' });
        }
  };


  // Get all wishlists (Only admin can access)
    module.exports.getAllWishLists = async (req, res) => {
        try {
        const wishlists = await wishList.find({});
        res.status(200).json(wishlists);
        } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Failed to get wishlists' });
        }
  };


